import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {VehicleHandlerService} from '../../shared/vehicle-handler.service';

@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit {

  carRegisterForm: FormGroup;
  errorMessage = false;

  constructor(public fb: FormBuilder, private handler: VehicleHandlerService) {
  }

  ngOnInit() {
    this.carRegisterForm = this.fb.group({
      manufacturer: ['', Validators.required],
      type: ['', Validators.required],
      color: [''],
      year: ['', Validators.required]
    });
    this.carRegisterForm.statusChanges.subscribe((status) => {
      if (status === 'VALID') {
        this.errorMessage = false;
      }
    });
  }

  onSubmit() {
    if (!this.carRegisterForm.valid) {
      this.errorMessage = true;
      return null;
    }
    this.handler.addVehicle(this.carRegisterForm.value);
    this.carRegisterForm.reset(this.carRegisterForm.value);
  }
}
