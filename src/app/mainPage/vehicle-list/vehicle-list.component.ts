import {Component, OnInit, ViewChild} from '@angular/core';
import {VehicleHandlerService} from '../../shared/vehicle-handler.service';
import {MatTable} from '@angular/material';

export interface VehicleData {
  position: number;
  manufacturer: string;
  type: string;
  color: string;
  year: string;
}

@Component({
  selector: 'app-vehicle-list',
  templateUrl: './vehicle-list.component.html',
  styleUrls: ['./vehicle-list.component.css']
})
export class VehicleListComponent implements OnInit {

  displayedColumns: string[] = ['position', 'manufacturer', 'type', 'color', 'year'];
  @ViewChild(MatTable) table: MatTable<any>;

  constructor(private vehicleHandler: VehicleHandlerService) { }

  ngOnInit() {
    this.vehicleHandler.connectMatTable(this.table);
  }

  getVehicles() {
    return this.vehicleHandler.vehicles;
  }

}
