import {TestBed} from '@angular/core/testing';

import {VehicleHandlerService} from './vehicle-handler.service';

describe('VehicleHandlerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VehicleHandlerService = TestBed.get(VehicleHandlerService);
    expect(service).toBeTruthy();
  });
});
