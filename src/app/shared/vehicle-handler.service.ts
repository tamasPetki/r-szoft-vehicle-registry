import {Injectable} from '@angular/core';
import {VehicleData} from '../mainPage/vehicle-list/vehicle-list.component';
import {MatTable} from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class VehicleHandlerService {

  private _vehicles: VehicleData[] = [];

  matTable: MatTable<any>;

  constructor() {
  }

  get vehicles(): VehicleData[] {
    return this._vehicles;
  }

  connectMatTable(element) {
    this.matTable = element;
  }

  addVehicle(vehicle) {
    vehicle.position = this._vehicles.length + 1;
    this._vehicles.push(vehicle);
    this.matTable.renderRows();
  }
}
